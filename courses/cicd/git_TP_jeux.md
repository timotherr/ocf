title:GIT - TP - Jeux
intro:nous permettra de manipuler les concepts vus en cours à l'aide de jeux.
conclusion:Fait le tour des fonctionnalités de base de git grâce à des jeux !

---

## Testons et observons

https://onlywei.github.io/explain-git-with-d3/

![center w80 hiddenpdf](images/git_explain_d3.png)
![center w100 hiddenhtml](images/git_explain_d3.png)

---

<!-- _class: hide-footer -->
## Learn Git Branching

https://learngitbranching.js.org

![center w60 hiddenpdf](images/git_learn.png)
![center w100 hiddenhtml](images/git_learn.png)

---

## Oh My Git

https://ohmygit.org/

![center w40 hiddenpdf](images/git_oh_my.png)
![center w100 hiddenhtml](images/git_oh_my.png)